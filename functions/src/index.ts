import * as functions from 'firebase-functions';

// Start writing Firebase Functions
// https://firebase.google.com/docs/functions/typescript

export const noCache = functions.https.onRequest((request, response) => {
  response.set('Cache-Control', 'no-cache');
});
