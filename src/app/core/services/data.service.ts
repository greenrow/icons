import { Injectable } from '@angular/core';
import { BASIC_ICONS as icons} from '@assets/static/basic_icons';
@Injectable({
  providedIn: 'root'
})
export class DataService {
  protected iconData: any = icons;
  constructor() {
    console.log('**', this.iconData)
  }

  getData() {
    return this.iconData;
  }
}
