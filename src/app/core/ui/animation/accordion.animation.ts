import { trigger, state, style, animate, transition, AnimationTriggerMetadata } from '@angular/animations';

export const accordionAnimation: AnimationTriggerMetadata = trigger('accordion',
  [
    state('closed', style({display: 'none',
         opacity: 0
    })
    ),
    state('open-grid', style({
       opacity: 1 })
    ),
    state('open', style({
      opacity: 1})
    ),

    state('arrow-up', style({
        transform: 'rotate(180deg)'
      })
    ),

    state('arrow-down', style({
      transform: 'rotate(0deg)'
      })
    ),

    transition('* => closed', [
      animate('0.4s')
    ]),
    transition('closed => open-grid', [style({display: 'grid'}),
        animate('0.6s')
    ]),
    transition('closed => open', [style({display: 'block'}),
        animate('0.6s')
    ]),
    transition('arrow-up <=> arrow-down', [
      animate('0.4s')
    ])
  ]
);
