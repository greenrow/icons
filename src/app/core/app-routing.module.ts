/**
 * Created by Aleksey Starzhinskiy on 03.04.2020.
 */
import { NgModule } from '@angular/core';
import {
  RouterModule, Routes,
} from '@angular/router';
import { CoreComponent } from './components/core/core.component';
import { IconsComponent } from '@pages/icons/components/icons.component';
import { SpinnerComponent } from '@core/components/test-spinner/spinner.component';


const Routes: Routes = [

  {
    path: '',
    component:  IconsComponent ,
  },
  {
    path: 'spinner',
    component: SpinnerComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      Routes,
    )
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {}
