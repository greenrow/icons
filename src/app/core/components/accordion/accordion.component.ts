import { Component, Input, Output, OnInit, OnChanges, EventEmitter } from '@angular/core';
import { accordionAnimation } from '@core/ui/animation/accordion.animation';

@Component({
    selector: 'app-accordion',
    templateUrl: 'accordion.component.html',
    animations: [accordionAnimation]
})


export class AccordionComponent implements OnInit, OnChanges {
    @Input()  show;
    @Input()  title: string;
    @Input()  displayType: string;
    @Output() toggle: EventEmitter<any> = new EventEmitter<any>();
    choosenStateType;
    state = 'open-grid';
    arrowState = 'arrow-up';

    constructor() { }

    initToggle() {
        this.arrowState = this.arrowState === 'arrow-up' ? 'arrow-down' : 'arrow-up';
        this.show = !this.show;
        this.state = this.show ? ( this.displayType === 'block' ? 'open' : 'open-grid') : 'closed';
        this.toggle.emit();
    }

    ngOnInit() {
      // check type
        if (this.displayType !== 'grid') {
            this.displayType = 'block';
        }
    }

    ngOnChanges(ev): void {
      if (ev.show && ev.show.currentValue) {
        if (!ev.show.firstChange) {
          this.arrowState = this.show  ?  'arrow-up' : 'arrow-down';
          this.state = this.show ? ( this.displayType === 'block' ? 'open' : 'open-grid') : 'closed';
        }
      }
    }
}
