import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {IconsModule} from '@pages/icons/icons.module';
import { AppRoutingModule } from './app-routing.module';
import { CoreComponent } from './components/core/core.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SpinnerComponent } from './components/test-spinner/spinner.component';

@NgModule({
  declarations: [
    CoreComponent,
    SpinnerComponent
  ],
  imports: [
    BrowserModule,
    IconsModule,
    AppRoutingModule,
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [CoreComponent]
})
export class AppModule { }
