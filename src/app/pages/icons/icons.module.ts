import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { IconsRoutingModule } from './icons-routing.module';
import { IconsComponent } from './components/icons.component';
import { SafePipe } from '@pipes/safe.pipe';
import { AccordionModule } from '@core/shared/accordion/accordion.module';



@NgModule({
  declarations: [
    IconsComponent,
    SafePipe
  ],
  imports: [
    CommonModule,
    FormsModule,
    IconsRoutingModule,
    AccordionModule
  ],
  exports: [
    IconsComponent
  ]

})
export class IconsModule { }
