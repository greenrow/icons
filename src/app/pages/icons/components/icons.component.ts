import { Component, OnInit, ViewChild } from '@angular/core';
import { DataService } from '@services/data.service';

@Component({
    selector: 'app-icons',
    templateUrl: './icons.component.html',
})
export class IconsComponent implements OnInit {
    public iconData: any = [];
    private iconDataCopy: any = [];
    public showIcon = false;
    public noIconData = false;
    public iconColorClass = 'ch-icon-color-default';

    public iconColorList = [
        {text: 'Default', class: 'default'},
        {text: 'Blue', class: 'blue'},
        {text: 'Green', class: 'green'},
        {text: 'Red', class: 'red'},
        {text: 'Orange', class: 'orange'}
    ];
    @ViewChild('dataIconRoot') dataRoot;

    constructor(private  dataService: DataService) {}

    changeIconColor(item) {
      this.iconColorClass = 'ch-icon-color-' + item.class;
    }

    searchIcons(e) {
          const searchText = e.target.value;
          let noDataVal = 0;
          if (searchText.length > 2) {
               const regExp = new RegExp(searchText, 'i');
               this.iconDataCopy.forEach((el, i) => {
                   this.iconData[i].noData = false;
                   this.iconData[i].icons =  el.icons.filter( (item) => {
                     return item.name.search(regExp) !== -1;
                   });
                   if (this.iconData[i].icons.length === 0) {
                      this.iconData[i].noData = true;
                      noDataVal++;
                    } else {
                      this.iconData[i].show = 'true';
                   }
                   this.noIconData = noDataVal === this.iconData.length;
               });
          } else if (searchText.length === 0) {
              this.noIconData = false;
              this.iconData.forEach((el, i) => {
                  el.noData = false;
                  el.showIcons = true;
                  el.icons = this.iconDataCopy[i].icons;
              });
          }
    }

    trackByIndex(index: number, value: any) {
      return index;
  }

    ngOnInit(): void {
        const data = this.dataService.getData();
        const regExp = /<svg.+ width=(['"])(\d+)\1.+/;
        const tmpIconObj = {};

        for (const item of Object.keys(data)) {
            let widthAttr;
            const regExpText = data[item].match(regExp);
            if (regExpText) {
                widthAttr = regExpText[2];
                if (tmpIconObj[widthAttr]) {
                  tmpIconObj[widthAttr].push({name: item, item: data[item]});
                } else {
                  tmpIconObj[widthAttr] = [];
                  tmpIconObj[widthAttr].push({name: item, item: data[item]});
                }
            }
        }

        // sort items
        for (const item of Object.keys(tmpIconObj)) {
             if (tmpIconObj[item].length > 1) {
                 tmpIconObj[item].sort((a, b) => {
                     if (a.name > b.name) {
                       return 1;
                     }
                 });
             }

           // add iitems
          this.iconData.push({size: item, show: true, icons: tmpIconObj[item]});
        }
        this.showIcon = true;

         // copyObj
        this.iconData.forEach((el, i) => {
            this.iconDataCopy[i] = {...el};
            this.iconDataCopy[i].icons = el.icons.map( (item) => {
                return item = {...item};
            });
        });
    }

}
