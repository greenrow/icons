/**
 * Created by Aleksey Starzhinskiy on 03.04.2020.
 */
import { NgModule } from '@angular/core';
import {
  RouterModule, Routes,
} from '@angular/router';


const Routes: Routes = [

];

@NgModule({
  imports: [
    RouterModule.forChild(
      Routes,
    )
  ],

  exports: [
    RouterModule
  ]

})
export class IconsRoutingModule {}
